﻿using AutoMapper;
using BaseAPI.Core.Domain;
using BaseAPI.Core.Repositories;
using BaseAPI.Infrastructure.Settings;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BaseAPI.Infrastructure.Repositories
{
    public class RedirectsRepository : IRedirectsRepository
    {
        private ConnectionStringsSettings _settings;
        private IMapper _mapper;

        public RedirectsRepository(ConnectionStringsSettings settings, IMapper mapper)
        {
            _settings = settings;
            _mapper = mapper;
        }

        public async Task<Redirect> GetAsync(int domainSystemId, int id)
        {
            using (IDbConnection db = new SqlConnection(_settings.CommonConnectionString))
            {
                var result = db.Query<Redirect>
                ("SELECT DomainSystemId, RedirectType, SourceTypeId, " +
                "Source, Target, CreateDate From dbo.InternalRedirect " +
                 "WHERE DomainSystemId = @domainSystemId AND id = @id",
                    new { domainSystemId = domainSystemId, id = id }).FirstOrDefault();

                return await Task.FromResult(result);
            }
        }

        public async Task<IEnumerable<Redirect>> GetByDomainAsync(int domainSystemId)
        {
            using (IDbConnection db = new SqlConnection(_settings.CommonConnectionString))
            {
                var result = db.Query<Redirect>
                ("SELECT DomainSystemId, RedirectType, SourceTypeId, " +
                "Source, Target, CreateDate From dbo.InternalRedirect " +
                 "WHERE DomainSystemId = @domainSystemId",
                    new { domainSystemId = domainSystemId }).ToList();

                return await Task.FromResult(result);
            }
        }

        public async Task InsertAsync(Redirect redirect)
        {
            using (IDbConnection db = new SqlConnection(_settings.CommonConnectionString))
            {
                await db.ExecuteAsync("INSERT INTO dbo.InternalRedirect (DomainSystemId, RedirectType, SourceTypeId, " +
                "Source, Target, CreateDate ) values (@DomainSystemId, @RedirectType, @SourceTypeId, @Source, @Target, @CreateDate)", redirect);
            }
            await Task.CompletedTask;
        }

        public async Task RmoveAsync(int id)
        {
            using (IDbConnection db = new SqlConnection(_settings.CommonConnectionString))
            {
                await db.ExecuteAsync("DELETE FROM dbo.InternalRedirect WHERE id = @id", new { id = id });
            }
            await Task.CompletedTask;
        }

        public async Task UpdateAsync(Redirect redirect)
        {
            using (IDbConnection db = new SqlConnection(_settings.CommonConnectionString))
            {
                await db.ExecuteAsync("UPDATE dbo.InternalRedirect SET DomainSystemId = @DomainSystemId, RedirectType = @RedirectType, " +
                    "SourceTypeId = @SourceTypeId, Source = @Source, Target = @Target, CreateDate = @CreateDate WHERE id = @id", redirect);
            }
            await Task.CompletedTask;
        }
    }
}