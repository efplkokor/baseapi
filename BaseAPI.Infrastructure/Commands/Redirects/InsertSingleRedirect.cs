﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseAPI.Infrastructure.Commands.Redirects
{
    public class InsertSingleRedirect : ICommand
    {
        public int DomainSystemId { get; set; }
        public int RedirectType { get; set; }
        public int SourceTypeId { get; set; }
        public string Source { get; set; }
        public string Target { get; set; }
    }
}
