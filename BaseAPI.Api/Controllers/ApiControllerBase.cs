﻿using Microsoft.AspNetCore.Mvc;
using BaseAPI.Infrastructure.Commands;
using System.Threading.Tasks;

namespace BaseAPI.Api.Controllers
{
    [Route("[controller]")]
    public class ApiControllerBase : Controller
    {
        private readonly ICommandDispatcher CommandDispatcher;

        protected ApiControllerBase(ICommandDispatcher commandDispatcher)
        {
            CommandDispatcher = commandDispatcher;
        }

        protected async Task DispatchAsync<T>(T command) where T : ICommand
        {
            await CommandDispatcher.DispatchAsync(command);
        }
    }
}