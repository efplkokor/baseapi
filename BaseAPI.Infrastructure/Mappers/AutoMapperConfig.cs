﻿using AutoMapper;
using BaseAPI.Core.Domain;
using BaseAPI.Infrastructure.Commands.Redirects;
using BaseAPI.Infrastructure.DTO;

namespace BaseAPI.Infrastructure.Mappers
{
    public static class AutoMapperConfig
    {
        public static IMapper Initialize()
            => new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Redirect, RedirectDto>();
                cfg.CreateMap<EditRedirect, Redirect>();

            })
            .CreateMapper();
    }
}