﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseAPI.Infrastructure.Commands.Redirects
{
    public class EditRedirect : ICommand
    {
        public int Id { get; set; }
        public int DomainSystemId { get; set; }
        public int RedirectType { get; set; }
        public int SourceTypeId { get; set; }
        public string Source { get; set; }
        public string Target { get; set; }
    }
}
