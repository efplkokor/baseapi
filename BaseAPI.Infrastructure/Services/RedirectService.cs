﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BaseAPI.Core.Domain;
using BaseAPI.Core.Repositories;
using BaseAPI.Infrastructure.DTO;

namespace BaseAPI.Infrastructure.Services
{
    public class RedirectService : IRedirectsService
    {
        private readonly IRedirectsRepository _redirectRepository;
        private readonly IMapper _mapper;

        public RedirectService(IRedirectsRepository redirectsRepository, IMapper mapper)
        {
            _redirectRepository = redirectsRepository;
            _mapper = mapper;
        }

        public async Task<RedirectDto> GetAsync(int domainSystemId, int id)
        {
            var redirect = await _redirectRepository.GetAsync(domainSystemId, id);
            var redirectDto = _mapper.Map<Redirect, RedirectDto>(redirect);

            return await Task.FromResult(redirectDto);
        }

        public async Task<IEnumerable<RedirectDto>> GetByDomainAsync(int domainSystemId)
        {
            var redirects = await _redirectRepository.GetByDomainAsync(domainSystemId);
            var redirectDto = _mapper.Map<IEnumerable<Redirect>, IEnumerable<RedirectDto>>(redirects);

            return await Task.FromResult(redirectDto);
        }

        public async Task InsertAsync(Redirect redirect)
        {
            await _redirectRepository.InsertAsync(redirect);

            await Task.CompletedTask;
        }

        public async Task RemoveAsync(int id)
        {
            await _redirectRepository.RmoveAsync(id);
            await Task.CompletedTask;
        }

        public async Task UpdateAsync(Redirect redirect)
        {
            await _redirectRepository.UpdateAsync(redirect);
            await Task.CompletedTask;
        }
    }
}
