﻿using System.Threading.Tasks;

namespace BaseAPI.Infrastructure.Commands
{
    public interface ICommandHandler<T> where T : ICommand
    {
        Task HandleAsync(T command);
    }
}