﻿using Autofac;
using Microsoft.Extensions.Configuration;
using BaseAPI.Infrastructure.Extensions;
using BaseAPI.Infrastructure.Settings;

namespace BaseAPI.Infrastructure.Ioc.Modules
{
    public class SettingsModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;

        public SettingsModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(_configuration.GetSettings<GeneralSettings>())
                .SingleInstance();
            builder.RegisterInstance(_configuration.GetSettings<ConnectionStringsSettings>())
                .SingleInstance();
        }
    }
}