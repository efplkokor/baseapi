﻿using AutoMapper;
using BaseAPI.Core.Domain;
using BaseAPI.Core.Repositories;
using BaseAPI.Infrastructure.Services;
using GenFu;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BaseAPI.Tests.Services
{
    public class RedirectsServiceTests
    {
        [Fact]
        public void get_async_should_return_single_record()
        {
            var redirect = A.New<Redirect>();

            var redirectRepositoryMock = new Mock<IRedirectsRepository>();
            var mapperMock = new Mock<IMapper>();
            redirectRepositoryMock.Setup(x => x.GetAsync(1, 1)).Returns(Task.FromResult(redirect));

            var redirectService = new RedirectService(redirectRepositoryMock.Object, mapperMock.Object);

            var result = redirectService.GetAsync(1, 1);

            // Assert
            //Assert.True(result.Source == redirect.Source);
        }
    }
}
