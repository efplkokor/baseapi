﻿namespace BaseAPI.Infrastructure.Exceptions
{
    public static class ErrorCodes
    {
        public static string InvalidDomain => "invalid_domain";
        public static string InvalidRedirect => "invalid_redirect";
    }
}