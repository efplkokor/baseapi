﻿using AutoMapper;
using BaseAPI.Core.Domain;
using BaseAPI.Infrastructure.Commands;
using BaseAPI.Infrastructure.Commands.Redirects;
using BaseAPI.Infrastructure.DTO;
using BaseAPI.Infrastructure.Exceptions;
using BaseAPI.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BaseAPI.Infrastructure.Handlers.Redirects
{
    public class EditRedirectHandler : ICommandHandler<EditRedirect>
    {
        IRedirectsService _redirectsService;
        IMapper _mapper;

        public EditRedirectHandler(IRedirectsService redirectsService, IMapper mapper)
        {
            _redirectsService = redirectsService;
            _mapper = mapper;
        }

        public async Task HandleAsync(EditRedirect command)
        {
            RedirectDto redirect = await _redirectsService.GetAsync(command.DomainSystemId, command.Id);

            if (redirect == null)
                throw new ServiceException(Exceptions.ErrorCodes.InvalidRedirect, $"There is no redirect with id {command.Id}", command);

            Redirect newRedirect = _mapper.Map<EditRedirect, Redirect>(command);

            // UpdateDate
            newRedirect.UpdateDate();

            await _redirectsService.UpdateAsync(newRedirect);
            await Task.CompletedTask;
        }
    }
}
