﻿using BaseAPI.Core.Domain;
using BaseAPI.Infrastructure.Commands;
using BaseAPI.Infrastructure.Commands.Redirects;
using BaseAPI.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BaseAPI.Infrastructure.Handlers.Redirects
{
    public class InsertSingleRedirectHandler : ICommandHandler<InsertSingleRedirect>
    {
        IRedirectsService _redirectsService;

        public InsertSingleRedirectHandler(IRedirectsService redirectsService)
        {
            _redirectsService = redirectsService;
        }

        public async Task HandleAsync(InsertSingleRedirect command)
        {
            Redirect redirect = Redirect.Create(command.DomainSystemId, command.RedirectType, command.SourceTypeId, command.Source, command.Target);
            await Task.Run( () => _redirectsService.InsertAsync(redirect));
        }
    }
}
