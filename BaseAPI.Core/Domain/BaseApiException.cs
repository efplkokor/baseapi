﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseAPI.Core.Domain
{
    public abstract class BaseAPIException : Exception
    {
        public string Code { get; }

        protected BaseAPIException()
        {
        }

        public BaseAPIException(string code)
        {
            Code = code;
        }

        public BaseAPIException(string message, params object[] args) : this(string.Empty, message, args)
        {
        }

        public BaseAPIException(string code, string message, params object[] args) : this(null, code, message, args)
        {
        }

        public BaseAPIException(Exception innerException, string message, params object[] args) 
            : this(innerException, string.Empty, message, args)
        {
        }

        public BaseAPIException(Exception innerException, string code, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            Code = code;
        }
    }
}
