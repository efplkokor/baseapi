﻿using BaseAPI.Core.Domain;
using BaseAPI.Infrastructure.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BaseAPI.Infrastructure.Services
{
    public interface IRedirectsService : IService
    {
        Task<RedirectDto> GetAsync(int domainSystemId, int id);

        Task<IEnumerable<RedirectDto>> GetByDomainAsync(int systemDomainId);

        Task InsertAsync(Redirect redirect);

        Task RemoveAsync(int id);

        Task UpdateAsync(Redirect redirect);
    }
}