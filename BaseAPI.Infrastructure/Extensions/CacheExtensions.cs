﻿using Microsoft.Extensions.Caching.Memory;
using System;

namespace BaseAPI.Infrastructure.Extensions
{
    public static class CacheExtensions
    {
        private static readonly TimeSpan DefaultCacheTime = TimeSpan.FromMinutes(15);
        private static string DefaultKey = "CacheKey";

        #region Default

        /// <summary>
        /// Store object in MemoryCache with DefaultCacheTime set to 15 minutes.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cache"></param>
        /// <param name="key">Store item in MemoryCache with specified key.</param>
        /// <param name="item">Object to store.</param>
        /// <param name="args">Params to add to key.</param>
        public static void StoreItem<T>(this IMemoryCache cache, string key, T item, params object[] args)
            => cache.Set<T>(GetKey(key, args), item, DefaultCacheTime);

        /// <summary>
        /// Get object from MemoryCache.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cache"></param>
        /// <param name="key">Store item in MemoryCache with specified key.</param>
        /// <param name="item">Object to store.</param>
        /// <param name="cacheTime">Time in minutes after object will be removed from MemoryCache.</param>
        /// <param name="args">Params to add to key.</param>
        public static void StoreItem<T>(this IMemoryCache cache, string key, int cacheTime, T item, params object[] args)
            => cache.Set<T>(GetKey(key, args), item, TimeSpan.FromMinutes(cacheTime));

        public static T GetItem<T>(this IMemoryCache cache, string key, params object[] args)
            => cache.Get<T>(GetKey(key, args));

        private static string GetKey(string key, params object[] args)
        {
            string parameters = string.Join("|", args);

            return $"{DefaultKey}{key}{parameters}";
        }

        #endregion Default
    }
}