﻿using Autofac;
using Microsoft.Extensions.Configuration;
using BaseAPI.Infrastructure.Ioc.Modules;
using BaseAPI.Infrastructure.Mappers;
using System;
using System.Collections.Generic;
using System.Text;

namespace BaseAPI.Infrastructure.Ioc
{
    public class ContainerModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;

        public ContainerModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(AutoMapperConfig.Initialize())
                .SingleInstance();
            builder.RegisterModule<CommandModule>();
            builder.RegisterModule<RepositoryModule>();
            builder.RegisterModule<ServiceModule>();
            builder.RegisterModule(new SettingsModule(_configuration));
        }
    }
}
