﻿using System;

namespace BaseAPI.Core.Domain
{
    public class Redirect
    {
        public int Id { get; private set; }
        public int DomainSystemId { get; private set; }
        public int RedirectType { get; private set; }
        public int SourceTypeId { get; private set; }
        public string Source { get; private set; }
        public string Target { get; private set; }
        public DateTime CreateDate { get; private set; }

        public Redirect()
        {
        }

        private Redirect(int domainSystemId, int redirectType, int sourceTypeId, string source, string target)
        {
            DomainSystemId = domainSystemId;
            RedirectType = redirectType;
            SourceTypeId = sourceTypeId;
            Source = source;
            Target = target;
            CreateDate = DateTime.Now;
        }

        public static Redirect Create(int domainSystemId, int redirectType, int sourceTypeId, string source, string target)
            => new Redirect(domainSystemId, redirectType, sourceTypeId, source, target);

        public void UpdateDate()
        {
            var date = DateTime.Now;

            if (CreateDate > date)
                throw new DomainException(ErrorCodes.CreateDate, "Record already updated", Id);

            CreateDate = date;
        }
    }
}