﻿using Microsoft.AspNetCore.Mvc;
using NLog;
using BaseAPI.Infrastructure.Commands;
using System;

namespace BaseAPI.Api.Controllers
{
    public class ConfigurationController : ApiControllerBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ConfigurationController(ICommandDispatcher commandDispatcher) : base(commandDispatcher)
        {
        }

        [HttpGet("/status")]
        public IActionResult GetBaseAPIStatus()
        {
            Logger.Info("Checking BaseAPI stauts...");

            string status = $"Status OK {DateTime.Now.ToString()}";

            return Json(status);
        }
    }
}