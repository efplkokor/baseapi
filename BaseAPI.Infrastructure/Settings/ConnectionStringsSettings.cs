﻿namespace BaseAPI.Infrastructure.Settings
{
    public class ConnectionStringsSettings
    {
        public string CommonConnectionString { get; set; }
    }
}