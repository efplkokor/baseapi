﻿using Microsoft.AspNetCore.Builder;

namespace BaseAPI.Api.Framework
{
    public static class Extensions
    {

        public static IApplicationBuilder UseExcetpionHandler(this IApplicationBuilder builder)
            => builder.UseMiddleware(typeof(ExceptionHandlerMiddleware));
    }
}