﻿using BaseAPI.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BaseAPI.Core.Repositories
{
    public interface IRedirectsRepository : IRepository
    {
        /// <summary>
        /// Get single redirect
        /// </summary>
        /// <param name="domainSystemId">For example DE - 1000001</param>
        /// <param name="id">Redirect id</param>
        /// <returns></returns>
        Task<Redirect> GetAsync(int domainSystemId, int id);

        /// <summary>
        /// Get redirects for domain
        /// </summary>
        /// <param name="domainSystemId"></param>
        /// <returns></returns>
        Task<IEnumerable<Redirect>> GetByDomainAsync(int domainSystemId);

        Task InsertAsync(Redirect redirect);

        Task UpdateAsync(Redirect redirect);

        Task RmoveAsync(int id);
    }
}
