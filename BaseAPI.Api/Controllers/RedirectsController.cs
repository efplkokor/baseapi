﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NLog;
using BaseAPI.Infrastructure.Commands;
using BaseAPI.Infrastructure.Commands.Redirects;
using BaseAPI.Infrastructure.DTO;
using BaseAPI.Infrastructure.Extensions;
using BaseAPI.Infrastructure.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BaseAPI.Api.Controllers
{
    [Route("[controller]")]
    public class RedirectsController : ApiControllerBase
    {
        private readonly IRedirectsService _redirectsService;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IMemoryCache _cache;

        public RedirectsController(IRedirectsService redirectsService, IMemoryCache cache, ICommandDispatcher commandDispatcher) : base(commandDispatcher)
        {
            _redirectsService = redirectsService;
            _cache = cache;
        }

        [HttpGet("{domainSystemId:int}/{id:int}")]
        public async Task<IActionResult> GetRedirect(int domainSystemId, int id)
        {
            var redirect = _cache.GetItem<RedirectDto>("Redirect", domainSystemId, id);

            if (redirect == null)
            {
                redirect = await _redirectsService.GetAsync(domainSystemId, id);
                _cache.StoreItem("Redirect", redirect, id);
            }

            return Json(redirect);
        }

        [HttpGet("{domainSystemId:int}")]
        public async Task<IActionResult> GetRedirectByDomain(int domainSystemId)
        {
            var redirect = _cache.GetItem<IEnumerable<RedirectDto>>("Redirect", domainSystemId);

            if (redirect == null)
            {
                redirect = await _redirectsService.GetByDomainAsync(domainSystemId);
                _cache.StoreItem("Redirect", redirect, domainSystemId);
            }

            return Json(redirect);
        }

        [HttpPost]
        public async void InsertSingleRedirect([FromBody]InsertSingleRedirect command)
        {
            await DispatchAsync(command);
        }

        [HttpDelete("{id:int}")]
        public async void Delete(int id)
        {
            await _redirectsService.RemoveAsync(id);            
        }

        [HttpPut]
        public async void Update([FromBody]EditRedirect command)
        {
            await DispatchAsync(command);
        }
    }
}