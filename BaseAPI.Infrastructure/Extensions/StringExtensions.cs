﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseAPI.Infrastructure.Extensions
{
    public static class StringExtensions
    {
        public static string Truncate(this string s, int length)
        {
            if (string.IsNullOrEmpty(s) || string.IsNullOrWhiteSpace(s)) return string.Empty;

            return s.Trim().Length > length ? s.Trim().Substring(0, length) : s.Trim();
        }
    }
}
